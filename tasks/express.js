"use strict";

var path = require('path');
var express = require('express');
var readline = require('readline');

function expressRedirector(status, to) {
	return function(req, res) {
		res.redirect(status, to);
		res.send();
	};
}

function afterListen (grunt, self, options, done) {
	if (options.keepalivePressenter) {
		var rl = readline.createInterface(process.stdin, process.stdout, null);
		console.log("Press enter to stop express");
		rl.on('line', function(line) {
			grunt.log.debug("Exiting grunt task " + line);
			done();
		});
	}
}

function serveExpress(grunt, self) {
	var options = self.options({
		port: 1027,
		paths: [{
			web: '/',
			fs: __dirname
		}],
		keepalive: true,
		keepalivePressenter: true
	}, self.data);
	grunt.log.debug("self: " + JSON.stringify(self));
	grunt.log.debug("options: " + JSON.stringify(options, null, 2));

	if (!options.paths) {
		console.error("no paths given");
	}

	if (!options.keepalive && options.keepalivePressenter) {
		console.warning("options.keepalivePressenter requires options.keepalive, disabled");
		options.keepalivePressenter = false;
	}

	var done = false;

	if (options.keepalive) {
		done = self.async();
	}

	var app = express();

	for (var serveIdx = 0; serveIdx < options.paths.length; serveIdx++) {
		var serve = options.paths[serveIdx];
		grunt.log.debug("serving: " + JSON.stringify(serve));

		switch (serve.type) {
			case "redirect":
				app.get(serve.web, expressRedirector(serve.status, serve.to));
				break;

			case "static":
				if (serve.web.substring(0,1) !== "/") {
					grunt.debug("webdir need to start with /, appended");
					serve.web = "/" + serve.web;
				}

				if (!serve.fs) {
					serve.fs = "";
				}

				if (serve.fs.substring(0,1) !== "/") {
					serve.fs = path.join(process.cwd(), serve.fs);
					grunt.log.debug("appending PWD to relative local path: " + serve.fs);
				}
				grunt.log.debug("Serving " + serve.fs + " on " + serve.web);
				app.use(serve.web, express.static(serve.fs, {
					etag: false
				}));
				break;

			default:
				grunt.log.error("unknown serve entity type " + serve.type + ", ignored");
		}
	}

	app.listen(options.port, function() {
		grunt.log.writeln("Express listens on port " + options.port);
		if (options.keepalive) {
			afterListen(grunt, self, options, done);
		}
	});
}

function registerExpressTask(grunt) {
	grunt.registerMultiTask("express", "Starts express server", function() {
		serveExpress(grunt, this);
	});
}

module.exports = registerExpressTask;
