# grunt-direct-express
Async Grunt task to call express with a configurable directory/webpath assignment.

You can start express from Grunt as an async task, but without forking a new node. It will
run parallel with the other tasks if needed.

You can set an option to use a "press enter to stop express" feature (in this case,
of course also the task won't end until you don't press enter).

An example:

    module.exports = function(grunt) {
      'use strict';
      grunt.initConfig({
      
        express : {

          dev : {
            options : {
              port : 8080,
              paths : [{
                type : "redirect",
                web : "/",
                status : 302,
                to : "/index.html"
              }, {
                type : "static",
                web : "/",
                fs: "./dev"
              }],
              keepalive : true,
              keepaliveKeypressenter : true
            }
          },

          dist : {
            options : {
              port : 8081,
              paths : [{
                type : "redirect",
                web : "/",
                status : 302,
                to : "/index.html"
              }, {
                type : "static",
                web : "/",
                fs : "./dist"
              }],
              keepalive : true,
              keepaliveKeypressenter : true
            }
          }

        }
      }
      grunt.loadNpmTasks("grunt-direct-express");
    };


## Explanation:

We define here two express tasks, a "dev" and a "dist" one. Grunt with
express:dev and express:dist will start an express instance using the
corresponding configuration.

All configuration is inside an "options:" element in the Grunt JSON, it is
an unfortunate syntactical limitation of the Grunt.

The valid options are these:

* port : we define the port on which express will listen(). The IP is always
         the default 0.0.0.0 (listen on all IPs). On the need it would be
         easy to change.
* keepalive : in nearly all the cases, it should be `true`. It is because
              this flag converts the task into an asynchronous one. **It also
              means, that the task won't end**.
* keepaliveKeypressenter : works only if keepalive is `true`. We will see a
                           "Press enter to stop express" message, and express
                           will run until we won't press enter. If we press,
                           express will stop, and the asynchronous Grunt task
                           will be solved ( see `async.done()` in Grunt docs).
* Paths : an array of the paths (web url - directory assigments). See next chapter.


## Path definitions

Actually, we have 2 path types:

* *redirect* types, these send http 30\* redirects on access. They work only on
  exact match, not for subdirectories. Both of them are self-explanatory in
  the Gruntfile example.
* *static* types, these simply send the files what they were asked for.

Note: `redirect` works only on exact matches and on `GET` queries, while `static`
  follow a prefix match!
